using Domain.Entities.Student;
using System.Threading.Tasks;
using Xunit;
using Moq;
using FluentAssertions;
using System.Linq.Expressions;
using System;

using Req = Application.Services.${ _opperationType}${ _resourceName}.${ _opperationType}${ _resourceName}
Req;
using Log = Domain.Entities.Log.${ _logName};

namespace Application.Tests.Unit.Services.${_opperationType }${ _resourceName}
{
    public partial class ${ _opperationType}${ _resourceName}
ServiceTests
{

    [Fact]
    public async Task ShouldCreateNew${ _resourceName}
    Async()
        {

        // 1. Arange            
        // 1.1 construct request object
        DateTimeOffset randomDateTime = GetRandomDateTime();
        var req = CreateRandomReq<Req>(randomDateTime);

        // 1.1 ${_resourceName}
        var random${ _resourceName} = CreateRandomEntity < Req, ${ _resourceName}> (req);
        var storage${ _resourceName} = random${ _resourceName};
        var expected${ _resourceName} = storage${ _resourceName};

        // 1.2 log 
        var randomLog = CreateRandomLog<Req, Log>(req);
        var inputLog = randomLog;
        var storageLog = randomLog;
        var expectedLog = storageLog;

        // 1.3 mock ${_.lowerFirst(_resourceName)} repo
        this.${ _.lowerFirst(_resourceName)}
        RepoMock.Setup(repo =>
repo.CreateAsync(It.IsAny <${ _resourceName}> ()))
                    .ReturnsAsync(storage${ _resourceName});

        // 1.4 mock log repo
        this.logRepoMock.Setup(repo =>
            repo.CreateAsync(It.IsAny<Log>()))
                .ReturnsAsync(storageLog);

        this.logRepoMock.Setup(repo =>
            repo.Update(It.IsAny<Log>()))
                .Returns(true);

        // 1.5 mock datetime broker
        this.dateTimeBrokerMock.Setup(broker =>
           broker.GetCurrentDateTime())
               .Returns(randomDateTime);

        // 2. Act
        var rsp = await ${ _.lowerFirst(opreationType)}${ _resourceName}
        Service.Process(req);

        // 3. Assert
        // 3.1 compare ${_.lowerFirst(_resourceName)}
        rsp.${ _resourceName}.Should().BeEquivalentTo(expected${ _resourceName});
        rsp.RspCode.Should().BeEquivalentTo("A");
        rsp.RspRef.Should().BeEquivalentTo(req.ReqRef);
        rsp.RspSysDate.Should().BeSameDateAs(randomDateTime);
        rsp.RspReason.Should().Be(null);
        rsp.RspMessage.Should().Be(null);
        rsp.ReqSysDate.Should().Be(randomDateTime);

        // 3.2 check method calls            
        this.logRepoMock.Verify(repo => repo.CreateAsync(It.IsAny<Log>()), Times.Once);
        this.logRepoMock.Verify(repo => repo.Update(It.IsAny<Log>()), Times.Once);
        this.${ _.lowerFirst(_resourceName)}
        RepoMock.Verify(repo => repo.CreateAsync(It.IsAny <${ _resourceName}> ()), Times.Once);
        this.loggingBrokerMock.Verify(x => x.LogInformation(It.IsAny<string>()), Times.Exactly(7));
        this.dateTimeBrokerMock.Verify(x => x.GetCurrentDateTime(), Times.Exactly(3));

        this.${ _.lowerFirst(_resourceName)}
        RepoMock.VerifyNoOtherCalls();
        this.logRepoMock.VerifyNoOtherCalls();
        this.dateTimeBrokerMock.VerifyNoOtherCalls();
        this.loggingBrokerMock.VerifyNoOtherCalls();
    }
}
}