namespace Application.Tests.Unit.Services.{{> upperFirstService}}
{
    public partial class {{> upperFirstService}}ServiceTests: ServiceTests
    {

        private readonly Mock<IBaseRepository<${_resourceName}>> ${_.lowerFirst(_resourceName)}RepoMock;
        private readonly Mock<IBaseRepository<${_logName}>> logRepoMock;
        private readonly Mock<ILoggingBroker> logger;        
        private readonly {{> upperFirstServiceInterface}} ${_.lowerFirst(opreationType)}${_resourceName}Service;
        private readonly Mock<IApplicationDbContext> ctx;

        public {{> upperFirstService}}ServiceTests()
        {
            this.${_.lowerFirst(_resourceName)}RepoMock = new Mock<IBaseRepository<${_resourceName}>>();
            this.ctx = new Mock<IApplicationDbContext>();
            this.logRepoMock = new Mock<IBaseRepository<${_logName}>>();
            this.logger = new Mock<ILoggingBroker>();

            this.${_.lowerFirst(opreationType)}${_resourceName}Service = new ${_opperationType}${_resourceName}Service(
                logger: this.logger.Object,
                ctx: this.ctx.Object,
                ${_.lowerFirst(_resourceName)}Repo: this.${_.lowerFirst(_resourceName)}RepoMock.Object,
                logRepo: this.logRepoMock.Object
                );            
        }
    }
}