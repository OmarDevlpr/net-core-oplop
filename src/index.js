const managers = {
    req: require('./req'),
    rsp: require('./rsp'),
    log: require('./log'),
    service: require('./service'),
    serviceInterface: require('./serviceInterface'),
    serviceHandler: require('./serviceHandler'),
    testCore: require('./test.core'),
    testDefault: require('./test.default'),
    testFunctional: require('./test.functional'),
    testIntegrity: require('./test.integrity')
}

module.exports = {managers};