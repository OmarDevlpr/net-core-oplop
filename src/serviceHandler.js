const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstServiceHandler;
    let upperFirstServiceHandler;

    if (operation === "lookup-many") {
        lowerFirstServiceHandler = `lookup${_.upperFirst(pluralize(model))}ServiceHandler`;
        upperFirstServiceHandler = `Lookup${_.upperFirst(pluralize(model))}ServiceHandler`;
    }
    else if (operation === "delete-all") {
        lowerFirstServiceHandler = `delete${_.upperFirst(pluralize(model))}ServiceHandler`;
        upperFirstServiceHandler = `Delete${_.upperFirst(pluralize(model))}ServiceHandler`;
    }
    else {
        lowerFirstServiceHandler = `${_.lowerFirst(operation)}${_.upperFirst(model)}ServiceHandler`;
        upperFirstServiceHandler = `${_.upperFirst(operation)}${_.upperFirst(model)}ServiceHandler`;
    }

    return {
        lowerFirstServiceHandler,
        upperFirstServiceHandler
    }
}

module.exports = {
    extrapolatePartials
} 