const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstReq;
    let upperFirstReq;

    if (operation === "lookup-many") {
        lowerFirstReq = `lookup${_.upperFirst(pluralize(model))}Req`;
        upperFirstReq = `Lookup${_.upperFirst(pluralize(model))}Req`;
    }
    else if (operation === "delete-all") {
        lowerFirstReq = `delete${_.upperFirst(pluralize(model))}Req`;
        upperFirstReq = `Delete${_.upperFirst(pluralize(model))}Req`;
    }
    else {
        lowerFirstReq = `${_.lowerFirst(operation)}${_.upperFirst(model)}Req`;
        upperFirstReq = `${_.upperFirst(operation)}${_.upperFirst(model)}Req`;
    }

    return {
        lowerFirstReq,
        upperFirstReq
    }
}

module.exports = {
    extrapolatePartials
} 