const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstServiceInterface;
    let upperFirstServiceInterface;

    if (operation === "lookup-many") {
        lowerFirstServiceInterface = `lookup${_.upperFirst(pluralize(model))}Service`;
        upperFirstServiceInterface = `ILookup${_.upperFirst(pluralize(model))}Service`;
    }
    else if (operation === "delete-all") {
        lowerFirstServiceInterface = `delete${_.upperFirst(pluralize(model))}Service`;
        upperFirstServiceInterface = `IDelete${_.upperFirst(pluralize(model))}Service`;
    }
    else {
        lowerFirstServiceInterface = `${_.lowerFirst(operation)}${_.upperFirst(model)}Service`;
        upperFirstServiceInterface = `I${_.upperFirst(operation)}${_.upperFirst(model)}Service`;
    }

    return {
        lowerFirstServiceInterface,
        upperFirstServiceInterface
    }
}

module.exports = {
    extrapolatePartials
} 