const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstTestCore;
    let upperFirstTestCore;

    if (operation === "lookup-many") {
        lowerFirstTestCore = `lookup${_.upperFirst(pluralize(model))}ServiceTests`;
        upperFirstTestCore = `Lookup${_.upperFirst(pluralize(model))}ServiceTests`;
    }
    else if (operation === "delete-all") {
        lowerFirstTestCore = `delete${_.upperFirst(pluralize(model))}ServiceTests`;
        upperFirstTestCore = `Delete${_.upperFirst(pluralize(model))}ServiceTests`;
    }
    else {
        lowerFirstTestCore = `${_.lowerFirst(operation)}${_.upperFirst(model)}ServiceTests`;
        upperFirstTestCore = `${_.upperFirst(operation)}${_.upperFirst(model)}ServiceTests`;
    }

    return {
        lowerFirstTestCore,
        upperFirstTestCore
    }
}

module.exports = {
    extrapolatePartials
} 