const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstLog;
    let upperFirstLog;

    if (operation === "lookup-many") {
        lowerFirstLog = `lookup${_.upperFirst(pluralize(model))}Log`;
        upperFirstLog = `Lookup${_.upperFirst(pluralize(model))}Log`;
    }
    else if (operation === "delete-all") {
        lowerFirstLog = `delete${_.upperFirst(pluralize(model))}Log`;
        upperFirstLog = `Delete${_.upperFirst(pluralize(model))}Log`;
    }
    else {
        lowerFirstLog = `${_.lowerFirst(operation)}${_.upperFirst(model)}Log`;
        upperFirstLog = `${_.upperFirst(operation)}${_.upperFirst(model)}Log`;
    }

    return {
        lowerFirstLog,
        upperFirstLog
    }
}

module.exports = {
    extrapolatePartials
} 