const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstRsp;
    let upperFirstRsp;

    if (operation === "lookup-many") {
        lowerFirstRsp = `lookup${_.upperFirst(pluralize(model))}Rsp`;
        upperFirstRsp = `Lookup${_.upperFirst(pluralize(model))}Rsp`;
    }
    else if (operation === "delete-all") {
        lowerFirstRsp = `delete${_.upperFirst(pluralize(model))}Rsp`;
        upperFirstRsp = `Delete${_.upperFirst(pluralize(model))}Rsp`;
    }
    else {
        lowerFirstRsp = `${_.lowerFirst(operation)}${_.upperFirst(model)}Rsp`;
        upperFirstRsp = `${_.upperFirst(operation)}${_.upperFirst(model)}Rsp`;
    }

    return {
        lowerFirstRsp,
        upperFirstRsp
    }
}

module.exports = {
    extrapolatePartials
} 