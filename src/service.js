const _ = require('lodash');
const pluralize = require('pluralize');


const extrapolatePartials = (model, operation, options) => {

    options = options || {};

    let lowerFirstService;
    let upperFirstService;

    if (operation === "lookup-many") {
        lowerFirstService = `lookup${_.upperFirst(pluralize(model))}Service`;
        upperFirstService = `Lookup${_.upperFirst(pluralize(model))}Service`;
    }
    else if (operation === "delete-all") {
        lowerFirstService = `delete${_.upperFirst(pluralize(model))}Service`;
        upperFirstService = `Delete${_.upperFirst(pluralize(model))}Service`;
    }
    else {
        lowerFirstService = `${_.lowerFirst(operation)}${_.upperFirst(model)}Service`;
        upperFirstService = `${_.upperFirst(operation)}${_.upperFirst(model)}Service`;
    }

    return {
        lowerFirstService,
        upperFirstService
    }
}

module.exports = {
    extrapolatePartials
} 