const managers = require('./index.js').managers;

module.exports = (plop, operation) => {

    return (context) => {

        const model = context.model;        
        const operations = context.operations;        
        
               
        const templateNames = ['Req', 'Rsp', 'Service', 'ServiceHandler', 'ServiceInterface', 'Log']        
        let actions = [];

         for (let operation of operations) {
            

             actions = [...actions, () => {

                 let partials = {};

                 for (let manager in managers) {                     
                     partials = { ...partials, ...managers[manager].extrapolatePartials(model, operation, {}) }                     
                 }

                 for (let partial in partials) {
                     plop.setPartial(partial, partials[partial]);
                 }

             }];

            // we also need the path for each  action
            actions = [...actions, ...templateNames.map(templateName => {
                return {
                    type: 'add',
                    path: process.cwd() + `/lab/{{> upperFirst${templateName}}}.cs`,
                    templateFile: process.cwd() + `/templates/${templateName}.cs`,
                }
            })]

                
        }
        return actions;       
    }
}