const _ = require('lodash');

const managers = require('./src/index').managers;
let newModelActions = require('./src/newModelActions')

module.exports = function (plop) {

    // controller generator
    plop.setGenerator('controller', {
        description: 'application controller logic',
        prompts: [
            {
                type: 'input',
                name: 'model',
                message: 'model name please'
            },
            {
                message: 'Select operation types',
                type: 'checkbox',
                choices: ["create", "lookup-many", "lookup", "update", "delete", "delete-all"],
                default: ["create", "lookup-many", "lookup", "update", "delete"],
                name: 'operations',
            }
        ],
        actions: newModelActions(plop, "create")            
        
    });


    plop.setActionType('registerPartials', function (answers, config, plop) {
                
        const log = upperFirstControllerName + 'Log';

        const upperFirstResourceName = _.upperFirst(model);
        const lowerFirstResourceName = _.lowerFirst(model);        
        const upperFirstLog = _.upperFirst(log);
        const lowerFirstLog = _.upperFirst(log);
        
        plop.setPartial('upperFirstResourceName', upperFirstResourceName);
        plop.setPartial('lowerFirstResourceName', lowerFirstResourceName);
        plop.setPartial('upperFirstLog', upperFirstLog);
        plop.setPartial('lowerFirstLog', lowerFirstLog);        

    });    

};